var x;
var team =[
{
    name:"director", 
    members:[
        {
            name: "David Jones",
            title: "Director sales"
        }
    ]
},
{
    name:"manager", 
    members:[
        {
            name: "John Smith",
            title: "Manager sales"
        },
        {
            name: "Daniel Miller",
            title: "Manager sales"
        }
    ]
},
{
    name:"exec", 
    members:[
        {
            name: "Paul Lopez",
            title: "Exec"
        },
        {
            name: "Kevin Brown",
            title: "Exec"
        },
        {
            name: "Jeff Taylor",
            title: "Exec"
        },
        {
            name: "Mark Lee",
            title: "Exec"
        }
    ]
}
];

var box = document.getElementById("members");

for (x in team) {
    if(team[x].name == 'director')
    {
        console.log(team[x].members);
        
        for (m in team[x].members) {
            box.innerHTML += '<div class="memberBox hexBg"><div class="hexBoxl"></div><div class="hexBoxm"><div class="heading">'+team[x].members[m].title+'</div></div><div class="hexBoxr"></div></div>'
        }
    }
    if(team[x].name == 'manager')
    {
        console.log(team[x].members);
        var pentagonBox = '<div class="memberBox pentagonBg">';
        var pentagon = "";
        for (m in team[x].members) {
            pentagon += '<div class="pentagon"><div class="heading pentagonTxt">'+team[x].members[m].title+'</div></div>';
        }

        box.innerHTML += pentagonBox + pentagon;
    }
    if(team[x].name == 'exec')
    {
        var circleBox = '<div class="memberBox" style=" justify-content: space-around;">'
        var circle = "";
        for (c in team[x].members) {
            circle += '<div class="circle"><div class="heading circleCenter">'+team[x].members[c].title+'</div></div>';
        }

        box.innerHTML += circleBox + circle;

    }
};

