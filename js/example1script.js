function openAutosomal(evt, AutosomalName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabBox");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tabButton");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" tabButtonActive", "");
    }
    document.getElementById(AutosomalName).style.display = "flex";
    evt.currentTarget.className += " tabButtonActive";
  }